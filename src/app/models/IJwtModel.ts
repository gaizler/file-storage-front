export interface IJwtModel{
    username:string,
    nameIdentifier:string,
    aud:string,
    iss:string,
    jti:string,
    sub:string,
    exp:string,
    roles:string
}
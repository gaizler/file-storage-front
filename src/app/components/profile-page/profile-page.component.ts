import { Component, OnInit } from '@angular/core';
import { IFileModel } from 'src/app/models/IFileModel';
import { AuthService } from 'src/app/services/auth.service';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css'],
})
export class ProfilePageComponent implements OnInit {
  fileList: IFileModel[];

  constructor(
    private _fileService: FileService,
    private _authService: AuthService
  ) {}

  ngOnInit(): void {}

  tabClick(tab: any) {
    this.update();
  }

  update() {
    this._fileService
      .getFilesForUser(this._authService.getCurrentUsername())
      .subscribe((data: IFileModel[]) => {
        this.fileList = data;
      });
  }
}

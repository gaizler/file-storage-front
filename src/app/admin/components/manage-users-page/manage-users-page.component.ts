import { Component, OnInit } from '@angular/core';
import { IUserModel } from 'src/app/models/IUserModel';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-manage-users-page',
  templateUrl: './manage-users-page.component.html',
  styleUrls: ['./manage-users-page.component.css'],
})
export class ManageUsersPageComponent implements OnInit {
  users: IUserModel[];
  constructor(
    private _authService: AuthService
  ) {}

  ngOnInit(): void {
    this.update();
  }

  update() {
    this._authService.getAllUsers().subscribe((data: IUserModel[]) => {
      this.users = data;
    });
  }
}

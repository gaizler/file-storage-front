import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileComponent } from './components/file/file.component';
import { FileListComponent } from './components/file-list/file-list.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';

@NgModule({
  declarations: [
    FileComponent,
    FileListComponent,
    NotFoundPageComponent,
  ],
  imports: [CommonModule],
  exports: [
    FileComponent,
    FileListComponent,
    NotFoundPageComponent,
  ],
})
export class SharedModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';
import { SearchFilesPageComponent } from './components/search-files-page/search-files-page.component';
import { ManageUsersPageComponent } from './components/manage-users-page/manage-users-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './components/user/user.component';
import { UserListComponent } from './components/user-list/user-list.component';

@NgModule({
  declarations: [
    SearchFilesPageComponent,
    ManageUsersPageComponent,
    UserComponent,
    UserListComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AdminLayoutComponent,
        children: [
          { path: '', redirectTo: '/admin/search', pathMatch: 'full' },
          { path: 'search', component: SearchFilesPageComponent },
          { path: 'users', component: ManageUsersPageComponent },
        ],
      },
    ]),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HttpClientModule,
  ],
})
export class AdminModule {}

import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { IFileModel } from '../models/IFileModel';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  private readonly _backendUrl: string = 'https://localhost:44300/api/';

  constructor(
    private _http: HttpClient,
    private _cookieService: CookieService
  ) {}

  uploadFile(form: FormData): Observable<any> {
    return this._http.post(this._backendUrl + 'files/upload', form, {
      reportProgress: true,
      observe: 'events',
      headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') },
    });
  }

  sendDownloadRequest(id: number): Observable<any> {
    return this._http.get(this._backendUrl + 'files/download/' + id, {
      reportProgress: true,
      observe: 'events',
      responseType: 'blob',
    });
  }

  downloadFile(data: HttpResponse<Blob>, file: IFileModel) {
    var blob = data.body as BlobPart;

    const downloadedFile = new Blob([blob], { type: data.body?.type });
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    a.download = file.title;
    a.href = URL.createObjectURL(downloadedFile);
    a.click();
    document.body.removeChild(a);
  }

  deleteFile(id: number) {
    return this._http.delete(this._backendUrl + 'files/' + id, {
      headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') },
    });
  }

  getFilesForUser(login: string): Observable<IFileModel[]> {
    return this._http.get<IFileModel[]>(
      this._backendUrl + 'files/' + login + '/all',
      { headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') } }
    );
  }

  getFileByUrl(url: string): Observable<IFileModel> {
    return this._http.get<IFileModel>(url, {
      headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') },
    });
  }

  searchFiles(request: string): Observable<IFileModel[]> {
    return this._http.get<IFileModel[]>(
      this._backendUrl + 'files/search?request=' + request,
      {
        headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') },
      }
    );
  }
}

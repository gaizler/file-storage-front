import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IFileModel } from 'src/app/models/IFileModel';
import { AuthService } from 'src/app/services/auth.service';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css'],
})
export class FileListComponent implements OnInit {

  @Input()
  fileList: IFileModel[];

  @Output()
  updateList = new EventEmitter()

  constructor(
    private _fileService: FileService,
    private _authService: AuthService
  ) {}

  ngOnInit(): void {
    this.update();
  }

   update() {
     this.updateList.emit()
   }
}


import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IFileModel } from 'src/app/models/IFileModel';
import { FileService } from 'src/app/services/file.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  isFound: boolean = false;
  file: IFileModel;
  form: FormGroup;

  constructor(
    private _notificationService: NotificationService,
    private _fileService: FileService
  ) {
    this.form = new FormGroup({
      link: new FormControl(null, Validators.compose([Validators.required])),
    });
    
  }

  ngOnInit(): void {}

  submit() {
    this._fileService.getFileByUrl(this.form.controls['link'].value).subscribe(
      (file: IFileModel) => {
        if (file) {
          this.isFound = true;
          this.file = file;
        }
      },
      () => {
        this.isFound = false;
        this._notificationService.showError('File not found');
      }
    );
  }

  update() {
    this.form.reset();
    this.isFound = false;
  }
}

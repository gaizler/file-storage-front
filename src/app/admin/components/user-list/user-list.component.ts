import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IUserModel } from '../../../models/IUserModel';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @Input()
  users:IUserModel[]

  @Output()
  updateList=new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

  update(){
    this.updateList.emit()
  }

}

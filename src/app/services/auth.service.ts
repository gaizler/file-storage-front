import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { IUserModel } from '../models/IUserModel';
import { IJwtModel } from '../models/IJwtModel';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly _backendUrl: string = 'https://localhost:44300/';

  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _cookieService: CookieService
  ) {}

  signup(_username: string, _password: String): Observable<string> {
    const body = { username: _username, password: _password };
    return this._http.post<string>(this._backendUrl + 'api/auth/signup', body);
  }

  login(_username: String, _password: String): Observable<string> {
    const body = { username: _username, password: _password };
    return this._http.post<string>(this._backendUrl + 'api/auth/signin', body);
  }

  getAllUsers(): Observable<IUserModel[]> {
    return this._http.get<IUserModel[]>(
      this._backendUrl + 'api/auth/all',
      {
        headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') },
      }
    );
  }

  makeAdmin(username: string) {
    return this._http.get(this._backendUrl + 'api/auth/makeadmin/' + username, {
      headers: { Authorization: 'Bearer ' + this._cookieService.get('jwt') },
    });
  }

  isLoggedIn(): boolean {
    if (this._cookieService.get('jwt')) {
      return true;
    }
    return false;
  }

  isAdmin(): boolean {
    if (!this.isLoggedIn()) return false;

    var token = this._cookieService.get('jwt');
    var jwtModel = jwtDecode<IJwtModel>(token);

    if (jwtModel.roles.includes('Admin')) {
      return true;
    }

    return false;
  }

  logout() {
    if (this._cookieService.check('jwt')) {
      this._cookieService.delete('jwt');
      this._router.navigate(['/login']);
    }
  }

  getCurrentUsername(): string {
    var token = this._cookieService.get('jwt');

    if (token) {
      var jwtModel = jwtDecode<IJwtModel>(token);
      return jwtModel.username;
    }
    return '';
  }
}

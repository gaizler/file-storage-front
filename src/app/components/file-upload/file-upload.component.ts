import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FileService } from 'src/app/services/file.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent implements OnInit {
  form: FormGroup;
  public progress: number = 0;
  public message: string = '';

  constructor(
    private _authService: AuthService,
    private _fileService: FileService,
    private _notificationService: NotificationService
  ) {
    this.form = new FormGroup({
      isPublic: new FormControl(),
      file: new FormControl(null, Validators.compose([Validators.required])),
    });
  }

  ngOnInit(): void {}

  fileChanged(files: any) {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];

    if (fileToUpload.size / 1000000 > 27) {
      this._notificationService.showError('Maximum file size is 27MB');
      this.form.reset();
    }
  }

  submit(files: any) {
    if (files.length === 0) {
      return;
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('username', this._authService.getCurrentUsername());
    formData.append(
      'isPublic',
      this.form.controls['isPublic'].value == null ? 'false' : 'true'
    );

    this._fileService.uploadFile(formData).subscribe(
      (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round((100 * event.loaded) / (event.total || 1));
          if (this.progress == 100)
            this.message = 'Wait a second till we finish...';
        } else if (event.type === HttpEventType.Response) {
          this._notificationService.showSuccess(
            'File was successfuly uploaded'
          );
          this.form.reset();
          this.progress = 0;
          this.message = '';
        }
      },
      (response: HttpErrorResponse) => {
        this._notificationService.showError(response.error);
        this.form.reset();
        this.progress = 0;
        this.message = '';
      }
    );
  }
}

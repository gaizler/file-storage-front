export interface IUserModel{
    userName:string,
    email?:string,
    roles?:string[],
}
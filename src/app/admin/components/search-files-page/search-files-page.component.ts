import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IFileModel } from 'src/app/models/IFileModel';
import { FileService } from 'src/app/services/file.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-search-files-page',
  templateUrl: './search-files-page.component.html',
  styleUrls: ['./search-files-page.component.css'],
})
export class SearchFilesPageComponent implements OnInit {
  form: FormGroup;

  isFound: boolean = false;
  searchResult: IFileModel[];

  constructor(
    private _notificationService: NotificationService,
    private _fileService: FileService
  ) {
    this.form = new FormGroup({
      search: new FormControl(null, Validators.compose([Validators.required])),
    });
  }

  ngOnInit(): void {}

  submit() {
    this._fileService.searchFiles(this.form.controls['search'].value).subscribe(
      (files: IFileModel[]) => {
        this.isFound = true;
        this.searchResult = files;
      },
      () => {
        this.isFound = false;
        this._notificationService.showError('Files not found');
      }
    );
  }

  update() {
    this._fileService.searchFiles(this.form.controls['search'].value).subscribe(
      (files: IFileModel[]) => {
        this.isFound = true;
        this.searchResult = files;
      },
      () => {
        this.isFound = false;
      }
    );
  }
}

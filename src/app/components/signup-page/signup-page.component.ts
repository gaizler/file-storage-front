import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.css'],
})
export class SignupPageComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _authService: AuthService,
    private _cookieService: CookieService,
    private _notificationService: NotificationService,
    private _router: Router
  ) {
    this.form = new FormGroup({
      username: new FormControl(
        null,
        Validators.compose([Validators.required])
      ),
      password: new FormControl(
        null,
        Validators.compose([Validators.required, Validators.minLength(8)])
      ),
    });
  }

  submit() {
    this._authService
      .signup(
        this.form.controls['username'].value,
        this.form.controls['password'].value
      )
      .subscribe(
        (data: string) => {
          this._cookieService.set('jwt', data);
          // this._fileOwnerService
          //   .addFileOwner({
          //     userName: this.form.controls['username'].value,
          //   })
          //   .subscribe();

          this._notificationService.showSuccess(
            'You was successfuly registered'
          );
          this._router.navigate(['/']);
        },
        (response: HttpErrorResponse) => {
          this._notificationService.showError(response.error);
        }
      );
  }

  ngOnInit(): void {}
}

import { HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';
import { IFileModel } from 'src/app/models/IFileModel';
import { AuthService } from 'src/app/services/auth.service';
import { FileService } from 'src/app/services/file.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css'],
})
export class FileComponent implements OnInit {
  private readonly _backendUrl: string = 'https://localhost:44300/api/';

  public message: string = '';
  public progress: number = 0;
  public isOwner: boolean = false;
  public isAdmin: boolean = false;

  @Input()
  file: IFileModel;

  @Output()
  updateList = new EventEmitter();

  constructor(
    private _fileService: FileService,
    private _clipboardService: ClipboardService,
    private _notificationService: NotificationService,
    private _authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isOwner = this.file.userLogin == this._authService.getCurrentUsername();
    this.isAdmin = this._authService.isAdmin();
  }

  download() {
    this._fileService.sendDownloadRequest(this.file.id).subscribe((event) => {
      if (event.type === HttpEventType.DownloadProgress) {
        this.progress = Math.round((100 * event.loaded) / event.total);
      } else if (event.type === HttpEventType.Response) {
        this.message = 'Download succeded.';
        this._fileService.downloadFile(event, this.file);
      }
    });
  }

  copyLink() {
    this._clipboardService.copyFromContent(
      this._backendUrl + 'files/' + this.file.id
    );
    this._notificationService.showSuccess(
      'Url was successfuly copied to clipboard'
    );
  }

  delete() {
    this._fileService.deleteFile(this.file.id).subscribe(
      () => {
        this.updateList.emit();
        this._notificationService.showSuccess('File was successfuly deleted');
      },
      () => {
        this._notificationService.showError('Server error');
      }
    );
  }
}

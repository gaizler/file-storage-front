import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IUserModel } from '../../../models/IUserModel';
import { IFileModel } from '../../../models/IFileModel';
import { AuthService } from '../../../services/auth.service';
import { FileService } from '../../../services/file.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  @Input()
  user: IUserModel;

  @Output()
  updateList = new EventEmitter();

  files: IFileModel[];
  needShowFiles: boolean = false;
  isAdmin: boolean | undefined = false;

  constructor(
    private _fileService: FileService,
    private _authService: AuthService,
    private _notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.isAdmin = this.user.roles?.includes('Admin');
  }

  makeAdmin() {
    this._authService.makeAdmin(this.user.userName).subscribe(
      () => {
        this.updateList.emit();
        this._notificationService.showSuccess('User role was updated');
      },
      () => {
        this._notificationService.showError('Server error');
      }
    );
  }

  update() {
    this._fileService
      .getFilesForUser(this.user.userName)
      .subscribe((data: IFileModel[]) => {
        this.files = data;
      });
  }

  showFiles() {
    this.needShowFiles = true;

    if (!this.files) {
      this.update();
    }
  }

  hideFiles() {
    this.needShowFiles = false;
  }
}

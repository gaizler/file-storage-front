export interface IFileModel{
    id:number,
    title:string,
    isPublic:boolean,
    userLogin:string,
    dateCreated:Date,
    fileUrl:string
}
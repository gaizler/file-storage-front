import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css'],
})
export class MainLayoutComponent implements OnInit {
  constructor(private _authService: AuthService) {}

  ngOnInit(): void {}

  getUserName() {
    return this._authService.getCurrentUsername();
  }

  isLogged(): boolean {
    return this._authService.isLoggedIn();
  }

  isAdmin(): boolean {
    return this._authService.isAdmin();
  }

  onLogout(event: Event): void {
    event.preventDefault();
    this._authService.logout();
  }
}
